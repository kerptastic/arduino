// Include the Servo library 
#include <Servo.h>

// servo arm limits: min=37, max=127

// servo setup: brown=ground, red=5v, orange=PIN (22)
// stepper setup: 
// silver button setup: red=from 5v, white=board ->PIN (28) ->10k, 10k=ground
// payload button setup:

/*
 * Encapsulates the functionality of a stepper motor.
 */
class MyStepper {
  public:
    MyStepper(int totalSteps, int stepPin, int dirPin) {
      m_totalSteps = totalSteps;
      m_stepPin = stepPin;
      m_dirPin = dirPin;

      init();
    }

    /*
     * Writes to the direction pin, and then steps the number of steps.
     * int dir: HIGH = clockwise, LOW = counter-clockwise
     */
    int write(int numSteps, int dir) {
      digitalWrite(m_dirPin, dir);

      for(int x = 0; x < numSteps; x++) {
        digitalWrite(m_stepPin, HIGH);
        delayMicroseconds(300);
        digitalWrite(m_stepPin, LOW);
        delayMicroseconds(300);
      }
    }
    
  private:
    int m_totalSteps;
    int m_stepPin;
    int m_dirPin;

    /*
     * Initialize the stepper motor device.
     */
    void init() {
      pinMode(m_dirPin, OUTPUT);
      pinMode(m_stepPin, OUTPUT);

      Serial.print("Stepper Motor Dir(");
      Serial.print(m_dirPin);
      Serial.print("), Step(");
      Serial.print(m_stepPin);
      Serial.println("): Init complete.");
    }
};

/*
 * Encapsulates the functionality of a servo motor.
 */
class MyServo {
  public:
    MyServo(int pin, int motorMin, int motorMax) {
      m_motor = Servo();
      m_pin = pin;
      m_min = motorMin;
      m_max = motorMax;

      init();
    }
    
    int getPin() { return m_pin; }
    
    int getPos() { return m_motor.read(); }
    
    int getMin() { return m_min; }
    int setMin(int min) { m_min = min; }
    
    int getMax() { return m_max; }
    int setMax(int max) { m_max = max; }

    /*
     * Writes to the stepper motor the angle to move to, and moves.
     */
    int write(int degrees) {
      int servoResult = 0;
      
      // increasing the servo pos
      if(degrees >= getPos() && degrees > m_max) {
        // dont push past the max
        Serial.println("OVER MAX - ADJUSTING TO MAX");
        m_motor.write(getMax());
        servoResult = -1;
      }
      // decreasing the servo pos
      else if (degrees <= getPos() && degrees < m_min) {
        // dont push past the min
        Serial.println("UNDER MIN - ADJUSTING TO MIN");
        m_motor.write(getMin());
        servoResult = -1;
      }
      else {
        Serial.println("MOTOR ON");
        m_motor.write(degrees);
      }

      Serial.print("Servo Motor Position: ");
      Serial.println(getPos());

      return servoResult;
    }

  private:
    Servo m_motor;
    int m_pin;
    int m_min;
    int m_max;
    int m_pos;

    /*
     * Initializes the stepper motor device.
     */
    void init() {
      m_motor.attach(m_pin);
      write(m_min);
      
      Serial.print("Servo Motor (");
      Serial.print(m_pin);
      Serial.println("): Init complete.");
     
      delay(1000);
    };
};
  
class MyButton {
  public:
    MyButton(int pin) {
      m_pin = pin;
      m_state = LOW;
      init();
    }

    int getPin() { return m_pin; }
    
    int getState() { return m_state; }
    void setState(int newState) { m_state = newState; }
    
  private:
    int m_pin;
    int m_state;
    
    /*
     * Initialize the button device.
     */
    void init() {
      pinMode(m_pin, INPUT);
      Serial.print("Button (");
      Serial.print(m_pin);
      Serial.println("): Init complete.");
    }
};

/*
 * 
 */
class GarbageArm {
  public:
    GarbageArm() {
      m_gripperState = 1; //start open
      m_hasPayload = false; //no payload

      init();
    }
    
    /*
     * Reads all of the available controls for state.
     */
    void readControls() {
      if (isButtonPressed(GRIPPER_CONTROL_BUTTON_ID) == HIGH) {
        Serial.println("OPERATE GRIPPER");
        operateGripper(GRIPPER_RANGE * m_gripperState, GRIPPER_SPEED * m_gripperState);
      }
    }

    /*
     * Operates the gripper arm running on the given servo motor
     * 
     */
    void operateGripper(int amt, int step) {
      MyServo *servo = m_servos[GRIPPER_MOTOR_ID];
      int newPos = servo->getPos();
      int finalPos = newPos + amt;
    
      finalPos = finalPos >= servo->getMax() ? servo->getMax() : finalPos;
      finalPos = finalPos <= servo->getMin() ? servo->getMin() : finalPos;
    
      Serial.print("Slerp - From(");
      Serial.print(newPos);
      Serial.print("), To(");
      Serial.print(finalPos);
      Serial.println(")");
    
      int servoResult = 0;
    
      // do/while loop used to handle both open and close, dont use for loop
      while(newPos != finalPos) {
    
        // if the gripper is open, then we want to move until the payload button
        // is detected - if the gripper is in the closed state, ignore the button
        if (m_gripperState == 1) {
          if(!isButtonPressed(GRIPPER_PAYLOAD_BUTTON_ID)) {
            servoResult = servo->write(newPos);
          } else {
            Serial.println("PAYLOAD DETECTED");
     
            // back off the server a couple clicks (tested manually)
            servo->write(servo->getPos() - (step*3));
    
            // set bad servo result so we dont tick the motor again
            servoResult = -1;
          }
        } else {
          servoResult = servo->write(newPos);
        }
    
        // detect if the servo is cannot move any further
        if (servoResult != 0) {
          break;
        }
        
        // delay for the motor and update the step
        delay(15);
        newPos += step;
      }
    
      // if we exited cleanly, then there is one more move to make
      if (servoResult == 0) {
        servo->write(newPos);
      }
    
      // swap gripper state
      m_gripperState *= -1;
      Serial.print("Gripper State: ");
      Serial.println(m_gripperState);
    }

  private:
    int m_gripperState;
    bool m_hasPayload;

    // configuration constants
    const static int NUM_SERVOS = 1;
    const static int NUM_STEPPERS = 1;
    const static int NUM_BUTTONS = 2;

    // gripper constants
    const static int GRIPPER_MOTOR_ID = 0;
    const static int GRIPPER_MOTOR_PIN = 22;
    
    const static int GRIPPER_CONTROL_BUTTON_ID = 0;
    const int GRIPPER_CONTROL_BUTTON_PIN = 28;
    
    const static int GRIPPER_PAYLOAD_BUTTON_ID = 1;
    const static int GRIPPER_PAYLOAD_BUTTON_PIN = 30;
    
    const static int GRIPPER_MIN = 52;
    const static int GRIPPER_MAX = 125;
    const static int GRIPPER_RANGE = GRIPPER_MAX - GRIPPER_MIN + 1;
    const static int GRIPPER_SPEED = 2;

    MyServo *m_servos[NUM_SERVOS];
    MyStepper *m_steppers[NUM_STEPPERS];
    MyButton *m_buttons[NUM_BUTTONS];

    /*
     * 
     */
    void init() {
      initButtons();
      initMotors();
    }

    /*
     * Checks whether or not the given button (via ID) is open or closed.
     */
    int isButtonPressed(int buttonId) {
      MyButton* button = m_buttons[buttonId];
    
      button->setState(digitalRead(button->getPin()));
      return button->getState();
    }

    /*
     * 
     */
    void initButtons() {
       // gripper control
      m_buttons[GRIPPER_CONTROL_BUTTON_ID] = new MyButton(GRIPPER_CONTROL_BUTTON_PIN);
    
      // gripper payload detector
      m_buttons[GRIPPER_PAYLOAD_BUTTON_ID] = new MyButton(GRIPPER_PAYLOAD_BUTTON_PIN);
    }

    /*
     * 
     */
    void initMotors() {
      m_servos[GRIPPER_MOTOR_ID] = new MyServo(GRIPPER_MOTOR_PIN, GRIPPER_MIN, GRIPPER_MAX);
    }
};

// global state
GarbageArm* garbageArm;

/*
 * One-time arduino setup.
 */
void setup() {
  Serial.begin(9600);

  garbageArm = new GarbageArm();
}

/*
 * Main arduino loop.
 */
void loop() { 
  garbageArm->readControls();
}
